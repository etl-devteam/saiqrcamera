package it.univet.saiqrcamera;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Toast;

import it.univet.visionar.visionarservice.VisionARService;

public class SplashActivity extends AppCompatActivity implements View.OnClickListener {

    VisionARService VARService = null;
    boolean mBound             = false;

    Button btn_enter;

    /** Defines callbacks for service binding, passed to bindService() */
    private final ServiceConnection binderConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder iBinder) {
            // We've bound to VisionARService, cast the IBinder and get VisionARService instance
            VisionARService.VARBinder binder = (VisionARService.VARBinder) iBinder;
            VARService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        initView();
    }

    private void initView() {
        btn_enter = findViewById(R.id.btn_press);
        btn_enter.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = new Intent(this, it.univet.visionar.visionarservice.VisionARService.class);
        startService(intent);

        bindService(intent, binderConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mBound = false;
        unbindService(binderConnection);
    }

    @Override
    public void onClick(View view) {
        if (mBound) {
            Bitmap bmpWelcomeImage = BitmapFactory.decodeResource(getResources(), R.drawable.first_image_visionar);

            if (VARService.WriteImg(bmpWelcomeImage) == VisionARService.writeStatus.SUCCESS) {
                Animation animZoomIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in);
                findViewById(R.id.visionar_image).startAnimation(animZoomIn);
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            } else {
                Toast.makeText(getBaseContext(), "You must connect VisionAR to proceed", Toast.LENGTH_LONG).show();
            }
        } else  Toast.makeText(getBaseContext(), "mbound: " + mBound, Toast.LENGTH_SHORT).show();
    }
}
