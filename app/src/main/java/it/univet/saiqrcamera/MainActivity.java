package it.univet.saiqrcamera;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageProxy;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.util.Size;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Task;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.mlkit.vision.barcode.Barcode;
import com.google.mlkit.vision.barcode.BarcodeScanner;
import com.google.mlkit.vision.barcode.BarcodeScannerOptions;
import com.google.mlkit.vision.barcode.BarcodeScanning;
import com.google.mlkit.vision.common.InputImage;

import java.lang.invoke.VolatileCallSite;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import it.univet.visionar.visionarservice.VisionARService;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, ImageAnalysis.Analyzer/*, CompoundButton.OnCheckedChangeListener */{

    VisionARService VARService = null;
    boolean mBound             = false;

    // Defines callbacks for service binding, passed to bindService()
    private final ServiceConnection binderConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder iBinder) {
            // We've bound to VisionARService, cast the IBinder and get VisionARService instance
            VisionARService.VARBinder binder = (VisionARService.VARBinder) iBinder;
            VARService = binder.getService();

            mBound = true;

            VARService.setTouchpadListener(ev -> {
                if (ev == VisionARService.touchpadEvent.SINGLE_TAP) {
                    Log.d("act list ", "Single Tap");
                } else if (ev == VisionARService.touchpadEvent.DOUBLE_TAP) {
                    Log.d("act list ", "Double Tap");
                } else if (ev == VisionARService.touchpadEvent.SWIPE_DOWN) {
                    Log.d("act list ", "Swipe Down");
                } else {  // VisionARService.touchpadEvent.SWIPE_UP
                    Log.d("act list ", "Swipe Up");
                }
            });

            //VARService.StartTouchpad();

            VARService.setVisionARListener(new VisionARService.VisionARListener() {
                @Override
                public void onConnect() {
                    Log.d("VisionAR Listener ", "VISIONAR CONNECTED");
                }

                @Override
                public void onDisconnect() {
                    Log.d("VisionAR Listener ", "VISIONAR DISCONNECTED");
                }
            });
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    // Useful to View
    private TableLayout tableLayoutItem;
    private CheckBox[] chkBox;
    private List<String> descriptionItem;
    private int index_list;

    /*Thread thread = new Thread(new Runnable() {
        @Override
        public synchronized void run() {
            try{

                Bitmap updateViewImage = LayoutToBitmap(R.layout.activity_main, R.id.itemTable);

                if (VARService.WriteImg(updateViewImage, VisionARService.includeBattery.INCLUDE, VisionARService.invertBitmap.INVERT) == VisionARService.writeStatus.SUCCESS) {
                    Log.d("@ @ @ ", "writeImage SUCCESS");
                } else Log.d("@ @ @ ", "writeImage FAILED");

                wait();

            } catch(Exception e) {
                Log.d("@ @ @ ", "Exception: " + e);
            }
        }
    });*/


    // Useful for Barcode Scanner by Google MLKit (Machine Learning Kit)
    private ListenableFuture<ProcessCameraProvider> cameraProviderFuture;
    private ExecutorService cameraExecutor;
    private PreviewView previewView;
    private String resultString;
    private int resultFormat;

    // Polling to visualize Layout directly on VisionAR display
    private static final int UPDATE_VIEW_TIMER_POLLING = 300;
    private final Handler updateViewHandler = new Handler();
    private Timer updateViewTimer;

    // ---------------- 'BEGIN OF PROGRAM' -------------------
    // onCreate, onDestroy, onResume, handleIntent and onNewIntent are always present in application to manage label, intent and others
    // Declaration, assignment of TextView, Button, ...
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();

        initData();
    }

    private void initView() {
        Bitmap image_intro  = BitmapFactory.decodeResource(getResources(), R.drawable.welcome_image);
        showStartDialog(image_intro);

        tableLayoutItem = findViewById(R.id.itemTable);
        previewView = findViewById(R.id.previewView);
        this.getWindow().setFlags(1024, 1024);

        Button resetButton = findViewById(R.id.resetBtn);
        resetButton.setOnClickListener(this);

        Bitmap bmp_1 = BitmapFactory.decodeResource(getResources(), R.drawable.headset_icon);
        Bitmap bmp_2 = BitmapFactory.decodeResource(getResources(), R.drawable.mouse_icon);
        Bitmap bmp_3 = BitmapFactory.decodeResource(getResources(), R.drawable.workstation_icon);

        descriptionItem = Arrays.asList("PZ-00Headset", "PZ-00Mouse", "PZ-00Workstation");
        List<Bitmap> iconItemList = new ArrayList<>();
        iconItemList.add(bmp_1);
        iconItemList.add(bmp_2);
        iconItemList.add(bmp_3);

        // Add Header First
        TableRow header = (TableRow) getLayoutInflater().inflate(R.layout.table_header, null);
        tableLayoutItem.addView(header);

        chkBox = new CheckBox[descriptionItem.size()];


        // Fill additional rows
        for (int i = 0; i < descriptionItem.size(); i++) {

            TableRow row = (TableRow) getLayoutInflater().inflate(R.layout.table_row, tableLayoutItem, false);
            chkBox[i] = row.findViewById(R.id.titleItem_check_row);
            //chkBox[i].setOnCheckedChangeListener(this);
            TextView descriptionItemRow = row.findViewById(R.id.titleItem_description_row);
            ImageView iconItemRow = row.findViewById(R.id.titleItem_icon_row);

            chkBox[i].setEnabled(false);
            chkBox[i].setChecked(false);
            descriptionItemRow.setText(descriptionItem.get(i));
            iconItemRow.setImageBitmap(iconItemList.get(i));
            tableLayoutItem.addView(row);
        }
    }

    private void initData() {

        cameraExecutor = Executors.newSingleThreadExecutor();
        cameraProviderFuture = ProcessCameraProvider.getInstance(this);

        cameraProviderFuture.addListener(() -> {
            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) != (PackageManager.PERMISSION_GRANTED)) {
                ActivityCompat.requestPermissions(MainActivity.this, new String[] {Manifest.permission.CAMERA}, 101);
            }
        }, ContextCompat.getMainExecutor(this));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 101 && grantResults.length > 0) {
            ProcessCameraProvider cameraProvider = null;
            try {
                cameraProvider = cameraProviderFuture.get();
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
            assert cameraProvider != null;
            bindPreview(cameraProvider);
        }
    }

    private void bindPreview(ProcessCameraProvider cameraProvider) {

        Preview preview = new Preview.Builder().build();
        CameraSelector cameraSelector = new CameraSelector.Builder().requireLensFacing(CameraSelector.LENS_FACING_BACK).build();

        preview.setSurfaceProvider(previewView.getSurfaceProvider());
        ImageCapture imageCapture = new ImageCapture.Builder().build();
        ImageAnalysis imageAnalysis = new ImageAnalysis.Builder()
                .setTargetResolution(new Size(1280, 720))
                .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                .build();
        imageAnalysis.setAnalyzer(cameraExecutor, this);
        cameraProvider.unbindAll();
        cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageCapture, imageAnalysis);
    }

    private void unbindCameraProvider() {
        ProcessCameraProvider cameraProvider = null;
        try {
            cameraProvider = cameraProviderFuture.get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        assert cameraProvider != null;
        cameraProvider.unbindAll();
    }

    private void wakeUpCameraProvider() {
        ProcessCameraProvider cameraProvider = null;
        try {
            cameraProvider = cameraProviderFuture.get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        assert cameraProvider != null;
        bindPreview(cameraProvider);
    }

    @Override
    public void analyze(ImageProxy imageProxy) {
        @SuppressLint("UnsafeOptInUsageError") Image mediaImage = imageProxy.getImage();

        if (mediaImage != null) {
            InputImage inputImage = InputImage.fromMediaImage(mediaImage, imageProxy.getImageInfo().getRotationDegrees());

            BarcodeScannerOptions barcodeScannerOptions = new BarcodeScannerOptions.Builder()
                    .setBarcodeFormats(Barcode.FORMAT_ALL_FORMATS)
                    .build();

            BarcodeScanner scanner = BarcodeScanning.getClient(barcodeScannerOptions);

            Task<List<Barcode>> result = scanner.process(inputImage)
                    .addOnSuccessListener(barcodes -> { // Task completed successfully
                        try {
                            resultString = barcodes.get(0).getRawValue();
                            resultFormat = barcodes.get(0).getFormat();     // You can get the Format of QRCode and refer to Barcode Android class
                        } catch (Exception e) {
                            e.printStackTrace();
                            resultString = null;
                        }
                        if(resultString != null) {
                            Log.d("@ @ @ ", "Result: " + resultString);
                            Log.d("@ @ @ ", "Code Format: " + resultFormat);
                            unbindCameraProvider();
                            handleDataResult(resultString);
                        } else {
                            Log.d("@ @ @ ", "No Code scanned");
                        }
                    })
                    .addOnFailureListener(e -> {    // Task failed with an exception
                    }).addOnCompleteListener(task -> imageProxy.close());
        }
    }

    private void handleDataResult(String dataStr) {

        if (containsSubString(descriptionItem, dataStr)) {
            if (isAlreadyScannedItem()) {
                showDialog("Already Scanned", "Item:\n" + dataStr + "\nis already scanned!\n", "\nPress OK to resume Bardcode scanner");
            } else {
                newItemShowDialog(dataStr);
            }
        } else {
            showDialog("Unknow Item", "Item:\n" + dataStr + "\nis NOT present in DataBase\n", "\nPress OK to resume Bardcode scanner");
        }
    }

    private boolean containsSubString(List<String> stringArray, String substring){
        index_list = 0;
        for (String string : stringArray){
            if (string.contains(substring)){
                return true;
            }
            index_list ++;
        }
        return false;
    }

    private boolean isAlreadyScannedItem() {
        return chkBox[index_list].isChecked();
    }

    private boolean isCompletedListItem() {
        int checkedBox = 0;
        for(CheckBox checkBox : chkBox){
            if (checkBox.isChecked()) checkedBox++;
        }

        return checkedBox >= chkBox.length;
    }

    private void resetCheckBox() {
        for(CheckBox checkBox : chkBox){
            checkBox.setChecked(false);
        }
    }

    @Override
    public void onClick(View view) {
        resetCheckBox();
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = new Intent(this, VisionARService.class);

        bindService(intent, binderConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();

        try {
            updateViewTimer.cancel();
        } catch (Exception e) {
            Log.d("@ @ @ ", "Exception: " + e);
        }

        //if (mBound) VARService.StopTouchpad();
        mBound = false;
        unbindService(binderConnection);
    }

    private void startSendPictureToVisionAR() {
        updateViewTimer = new Timer();
        updateViewTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                // In case of Thread you could use Mutex and
                // block the thread with mutex and next
                // disblock the thread in dispatchDraw of MyView
                updateViewHandler.post(() -> {
                    try{
                        Bitmap updateViewImage = LayoutToBitmap(R.layout.activity_main, R.id.itemTable);

                        if (VARService.WriteImg(updateViewImage, VisionARService.includeBattery.PLAIN, VisionARService.invertBitmap.INVERT) == VisionARService.writeStatus.SUCCESS) {
                            Log.d("@ @ @ ", "writeImage SUCCESS");
                        } else Log.d("@ @ @ ", "writeImage FAILED");

                    } catch(Exception e) {
                        Log.d("@ @ @ ", "Exception: " + e);
                    }
                });
            }
        }, 0, UPDATE_VIEW_TIMER_POLLING);
    }

    private void sendPictureToVisionAR() {

        try{
            Bitmap updateViewImage = LayoutToBitmap(R.layout.activity_main, R.id.itemTable);

            if (VARService.WriteImg(updateViewImage, VisionARService.includeBattery.PLAIN, VisionARService.invertBitmap.INVERT) == VisionARService.writeStatus.SUCCESS) {
                Log.d("@ @ @ ", "writeImage SUCCESS");
            } else Log.d("@ @ @ ", "writeImage FAILED");

        } catch(Exception e) {
            Log.d("@ @ @ ", "Exception: " + e);
        }
    }

    private void showStartDialog(Bitmap intro) {

        androidx.appcompat.app.AlertDialog.Builder mBuilder = new androidx.appcompat.app.AlertDialog.Builder(MainActivity.this);
        mBuilder.setIcon(R.drawable.qrscan);
        mBuilder.setTitle("QRScanner");
        mBuilder.setMessage("Press OK to start Barcode Reader");
        mBuilder.setCancelable(false);
        mBuilder.setPositiveButton("OK", (dialogInterface, i) -> {
            if (VARService.WriteImg(intro) == VisionARService.writeStatus.SUCCESS){
                wakeUpCameraProvider();
                //thread.start();
                startSendPictureToVisionAR();
                Log.d("@ @ @ ", "writeImage SUCCESS");
            } else Log.d("@ @ @ ", "writeImage FAILED");
            dialogInterface.dismiss();
        });

        AlertDialog versionDialog = mBuilder.create();
        versionDialog.show();
    }

    private void showDialog(String title_str, String result_str, String message_str) {

        androidx.appcompat.app.AlertDialog.Builder mBuilder = new androidx.appcompat.app.AlertDialog.Builder(MainActivity.this);
        mBuilder.setIcon(R.drawable.qrscan);
        mBuilder.setTitle(title_str);
        mBuilder.setMessage(result_str + "\n" + message_str);
        mBuilder.setCancelable(false);
        mBuilder.setPositiveButton("OK", (dialogInterface, i) -> {
            wakeUpCameraProvider();
            if (isCompletedListItem()) {
                resetCheckBox();
            }
            dialogInterface.dismiss();
        });

        AlertDialog versionDialog = mBuilder.create();
        versionDialog.show();
    }

    private void newItemShowDialog(String dataStr) {

        androidx.appcompat.app.AlertDialog.Builder mBuilder = new androidx.appcompat.app.AlertDialog.Builder(MainActivity.this);
        mBuilder.setIcon(R.drawable.qrscan);
        mBuilder.setTitle("New Item");
        mBuilder.setMessage("Item:\n" + dataStr + "\n" + "\nWant to add the new item?");
        mBuilder.setCancelable(false);
        mBuilder.setPositiveButton("OK", (dialogInterface, i) -> {

            chkBox[index_list].setChecked(true);
            //setCheck();

            if (!isCompletedListItem()) wakeUpCameraProvider();
            dialogInterface.dismiss();
        });
        mBuilder.setNegativeButton("CANCEL", (dialogInterface, i) -> {
            wakeUpCameraProvider();
            dialogInterface.dismiss();
        });
        mBuilder.setOnDismissListener(dialogInterface -> {
            if (isCompletedListItem()) {
                showDialog("List Completed", "You have scanned all item!", "\nOK to clear the list and restart");
            }
        });

        AlertDialog versionDialog = mBuilder.create();
        versionDialog.show();
    }

    // ---------------- 'BACKGROUND ANIMATION ON GLASSES' -------------------

    // activityLayout is the current activity, selectedView is the interested Layout View.
    // Layout View is converted into Bitmap in order to be shown on VisionAR display
    private Bitmap LayoutToBitmap(int activityLayout, int selectedView) {



        LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        ViewGroup layout = (ViewGroup) inflater.inflate(activityLayout, null);

        layout.setDrawingCacheEnabled(true);
        Bitmap bmpFromView = getBitmapFromView(this.getWindow().findViewById(selectedView));
        layout.setDrawingCacheEnabled(false);

        return bmpFromView;
    }

    // It is called from TableLayout_to_Bitmap and it is used to draw layout on canvas in order to write layout itself on returned bitmap
    private Bitmap getBitmapFromView(View view) {

        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable!=null){
            bgDrawable.draw(canvas);
        } else{
            canvas.drawColor(Color.WHITE);
        }
        view.draw(canvas);


        return returnedBitmap;
    }



    /*
    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        Toast.makeText(this, "Ciao", Toast.LENGTH_SHORT).show();

        //sendPictureToVisionAR();
    }*/

    /*private synchronized void setCheck() {
        chkBox[index_list].setChecked(true);

        notifyAll();
    }*/


    // ---------------- END 'BACKGROUND ANIMATION ON GLASSES' -------------------
}