package it.univet.saiqrcamera;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.concurrent.Semaphore;

class MyView extends FrameLayout {

    //public Semaphore sem = new Semaphore(1);

    public MyView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected synchronized void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);

        notifyAll();
    }
}
